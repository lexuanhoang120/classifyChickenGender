# Import required Libraries
import time
from threading import Thread
from tkinter import *

import cv2
import numpy as np
import torch
from PIL import Image
from PIL import ImageTk
from keras import models


# from utils.general import LOGGER
# from opt_einsum.backends import torch
# from utils.torch_utils import time_sync

def time_sync():
    # pytorch-accurate time
    if torch.cuda.is_available():
        torch.cuda.synchronize()
    return time.time()


# Create an instance of TKinter Window or frame
window = Tk()
window.title("Classification of the chicken gender")
# Set the size of the window
window.geometry("810x700")

# Create a Label to capture the Video frames
label = Label(window)
label.grid(row=0, column=0)
video = cv2.VideoCapture(0)
video.set(5, 25)
model = models.load_model('model.h5')


# Define function to show frame
def show_frames():
    # Get the latest frame and convert into Image
    cv2image = cv2.cvtColor(video.read()[1], cv2.COLOR_BGR2RGB)
    img = Image.fromarray(cv2image)
    # Convert image to PhotoImage
    imgtk = ImageTk.PhotoImage(image=img)
    label.imgtk = imgtk
    label.configure(image=imgtk)
    # Repeat after an interval to capture continiously
    label.after(20, show_frames)


show_frames()

gender = Label(window, text=" Gender", bd='10', font=('Helvetica bold', 100), width=10)
gender.grid(row=1, column=0)


def predict():
    while True:
        t1 = time_sync()
        _, frame = video.read()

        # Convert the captured frame into RGB
        im = Image.fromarray(frame, 'RGB')

        # Resizing into 128x128 because we trained the model with this image size.
        im = im.resize((224, 224))
        img_array = np.array(im)

        # Our keras model used a 4D tensor, (images x height x width x channel)
        # So changing dimension 128x128x3 into 1x128x128x3
        img_array = np.expand_dims(img_array, axis=0)

        # Calling the predict method on model to predict 'me' on the image
        prediction = round(model.predict(img_array)[0][0])
        # print(prediction)
        # if prediction is 0, which means I am missing on the image, then show the frame in gray color.
        t2 = time_sync()
        if prediction == 1:
            # frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            gender.configure(text="Hen")
            # print("1")
        else:
            gender.configure(text="Cock")
            # print("1")
        print(f"Speed: {t2 - t1}")


thread = Thread(target=predict)
thread.start()

window.mainloop()
